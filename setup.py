#!/usr/bin/python
from setuptools import setup, find_packages
import cronschedule

setup(name='cronschedule',
      version=cronschedule.__version__,
      description = 'Simple CRON expression python package',
      packages = find_packages(),
      author = 'Rick (blokje) Voormolen',
      author_email = 'rick@voormolen.org',
      url = 'https://www.bitbucket.org/rvoormolen/cronschedule',

      classifiers = [
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 2.7',
        'Topic :: System :: Shells',
        'Topic :: Utilities'
      ]
)
