cronschedule
============

CronSchedule is a python module to help out with parsing cron expressions,
originally it is based on a PHP implementation found at [GitHub] [1]

Cron Expression
===============
A normal cron expression is formatted in 5 fields

* minute of hour
* hour of day
* day of month
* month
* day of week (0 - Sunday, 1 - Monday ...)

## Preconfigured expressions
There are a few preconfigured expressions, which can also be used as expression

* @yearly, @annually - Run once a year, midnight, Jan. 1 - 0 0 1 1 *
* @monthly - Run once a month, midnight, first of month - 0 0 1 * *
* @weekly - Run once a week, midnight on Sun - 0 0 * * 0
* @daily - Run once a day, midnight - 0 0 * * *
* @hourly - Run once an hour, first minute - 0 * * * *

Installation
============
    sudo python setup.py install

Examples
==========

    import cronschedule
    import datetime

    # Works with predefined scheduling definitions
    cron = cronschedule.factory('@daily')
    print cron.get_next_run_date().isoformat()
    print cron.get_previous_run_date().isoformat()

    # Works with complex expressions
    cron = cronschedule.factory('3-59/15 2,6-12 */15 1 2-5')
    print cron.get_next_run_date().isoformat()

    # Calculate a run date two iterations in the future
    cron = cronschedule.factory('@daily')
    print cron.get_next_run_date(nth=2).isoformat()

    # Calculate a run date relative to a specific time
    cron = cronschedule.factory('@monthly')
    specific_time = datetime.datetime(2010, 1, 12, 0, 0, 0)
    print cron.get_next_run_date(specific_time).isoformat()

References
==========
[1]: https://github.com/mtdowling/cron-expression/ "mtdownling/cron-expression"




