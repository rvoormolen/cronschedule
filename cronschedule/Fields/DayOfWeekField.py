import AbstractField
import datetime
from calendar import monthrange

class DayOfWeekField(AbstractField.AbstractField):
    """
    Day of week field. Allows: * / , - ? L #

    Days of the week can be represented as a number 0-7 (0|7 = Sunday)
    or as a three letter string: SUN, MON, TUE, WED, THU, FRI, SAT.

    'L' stands for "last". It allows you to specify constructs such as
    "the last Friday" of a given month.

    '#' is allowed for the day-of-week field, and must be followed by a
    number between one and five. It allows you to specify constructs such as
    "the second Friday" of a given month.

    Based on https://github.com/mtdowling/cron-expression/blob/master/src/Cron/DayOfWeekField.php
    """
    __validate__ = "[-*,/0-9A-Z]+"

    _WEEKDAYS = {
        'MON': '1',
        'TUE': '2',
        'WED': '3',
        'THU': '4',
        'FRI': '5',
        'SAT': '6',
        'SUN': '0'
    }

    def _weekday_convert(self, weekday, to_cronjob = True):
        """
        Convert Python weekday to Cronjob style weekday

        Cronjob weekday numbering (Sun = 0, Mon = 1)
        Python weekday number     (Sun = 6, Mon = 0)

        Python to Cronjob
        weekday = weekday+1

        Cronjob to Python
        weekday = weekday-1
        """
        if( type(weekday) != int ):
            raise ValueError('weekday should be a number')
            
        if(to_cronjob == True):
            weekday = weekday+1
        elif(to_cronjob == False):
            weekday = weekday-1

        if(weekday > 6):
            weekday = weekday - 7
        if(weekday < 0):
            weekday = weekday + 7

        return weekday

    def isSatisfiedBy(self, date, value):
        if( type(date) != datetime.datetime ):
            raise ValueError('date should be in datedate.datetime format')

        ## ? states that the field is to be skipped
        if ( value == '?' ):
            return True

        ## Convert text day of the week to integer
        for w in self._WEEKDAYS:
            value = value.upper().replace(w, self._WEEKDAYS[w])

        currentYear     = date.year
        currentMonth    = date.month
        lastDayOfMonth  = monthrange(date.year, date.month)[1] 
        lastDateOfMonth = datetime.datetime(currentYear, currentMonth, lastDayOfMonth)

        ## Get supplied DayOfWeek and convert it to CronJob numbering
        dayOfWeek = self._weekday_convert(date.weekday(), to_cronjob = True)

        ## Find out if this is the last specific weekday of this month
        if( value.count('L') == 1 ):
            # Parse required weekday and convert it to Python
            day_num = self._weekday_convert(int(value.split('L')[0]), to_cronjob=False)
            return ( date.weekday() == day_num ) and ((lastDateOfMonth - date).days < 7 )

        ### Handle # hash tokens
        if( value.count('#') == 1):
            (weekday, nth) = value.split('#')
            weekday = self._weekday_convert(int(weekday), to_cronjob = False)
            nth = int(nth)
            if(weekday > 4):
                raise ValueError('Weekday must be a value between 1 and 5')

            if(nth > 5):
                raise ValueError('It is impossible to have more than 5 occurences of a specific weekday')

            # The current weekday must match the targeted weekday to proceed
            if(date.weekday() != weekday):
                return False

            # Find first monthly occurance of the specific weekday
            tdate = datetime.datetime(date.year, date.month, 1)
            while(tdate.weekday() != weekday):
                tdate = datetime.datetime(date.year, date.month, tdate.day+1)
       
            # Projected date = first occurance + nth-1 weeks later
            tdate = datetime.datetime(date.year, date.month, tdate.day+((nth-1)*7))
            return (date == tdate)

        return self.isSatisfied( dayOfWeek, value )

    def increment(self, date, invert = False):
        if(type(date) != datetime.datetime):
            raise ValueError()

        if(invert == True):
            date = date-datetime.timedelta(days = 1)
            date = date.replace(hour = 23, minute = 59, second = 0)
        else:
            date = date+datetime.timedelta(days = 1)
            date = date.replace(hour = 0, minute = 0, second = 0)

        return date
   
__all__ = [ 'DayOfWeekField' ]
