import AbstractField
import datetime
from calendar import monthrange

class DayOfMonthField(AbstractField.AbstractField):
    """
    Day of month field. Allows: * , / - ? L W

    'L' stands for "last" and specifies the last day of the month.

    The 'W' character is used to specify the weekday (Monday-Friday) nearest the
    given day. As an example, if you were to specify "15W" as the value for the
    day-of-month field, the meaning is: "the nearest weekday to the 15th of the
    month". So if the 15th is a Saturday, the trigger will fire on Friday the
    14th. If the 15th is a Sunday, the trigger will fire on Monday the 16th. If
    the 15th is a Tuesday, then it will fire on Tuesday the 15th. However if you
    specify "1W" as the value for day-of-month, and the 1st is a Saturday, the
    trigger will fire on Monday the 3rd, as it will not 'jump' over the boundary
    of a month's days. The 'W' character can only be specified when the
    day-of-month is a single day, not a range or list of days.

    Based on https://github.com/mtdowling/cron-expression/blob/master/src/Cron/DayOfMonthField.php
    """
    __validate__ = "[*,/-?LW0-9A-Za-z]+"

    def getNearestWeekday(self, currentYear, currentMonth, targetDay):
        """
        Get the nearest day of the week for a given day in a month

        Hence NOT Saturday or Sunday but the day closest to this

        @param int currentYear  Current Year
        @param int currentMonth Current Month
        @param int targetDay    Target day of the month

        @return datetime.date   The nearest date
        """
        target = datetime.datetime.strptime("%04d-%02d-%02d" % ( currentYear, currentMonth, targetDay ),  '%Y-%m-%d')


        # Mon = 0, Sun = 6
        if(target.weekday() < 5):
            return target

        days_in_month = monthrange(target.year, target.month)[1]

        for i in [-1, 1, -2, 2]:
            adj_targetDay = targetDay + i
            if(adj_targetDay > 0) and (adj_targetDay <= days_in_month):
                target = datetime.datetime.strptime("%04d-%02d-%02d" % ( currentYear, currentMonth, adj_targetDay ),  '%Y-%m-%d')
                if(target.weekday() < 5):
                    return target

    def isSatisfiedBy(self, date, value):
        if( type(date) != datetime.datetime ):
            raise ValueError('date should be in datedate.datetime format')

        ## ? states that the field is to be skipped
        if ( value == '?' ):
            return True

        ## Check to see if this is the last day of the month
        if ( value == 'L' ):
            return monthrange(date.year, date.month)[1] == date.day

        ## Check to see if this is the nearest weekday to a particular value 
        if ( value.count('W') > 0 ):
            return ( date == self.getNearestWeekday( date.year, date.month, int(value.split('W')[0]) ) )

        ## Check if the day satisfies the value
        return self.isSatisfied( date.day, value )

    def increment(self, date, invert = False):
        if(type(date) != datetime.datetime):
            raise ValueError()

        if(invert == True):
            date = date-datetime.timedelta(days = 1)
            date = date.replace(hour = 23, minute = 59, second = 0)
        else:
            date = date+datetime.timedelta(days = 1)
            date = date.replace(hour = 0, minute = 0, second = 0)

        return date
    
__all__ = [ 'DayOfMonthField' ]
