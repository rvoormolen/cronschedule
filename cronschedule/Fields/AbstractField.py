"""
AbstactField module
"""

import re

class AbstractField:
    __validate__ = ''

    def __init__(self):
        self._validate = re.compile(self.__validate__)

    def isSatisfied(self, date_value, value):
        """
        Check to see if a field is satisfied by a value

        @param string int       Date value to check
        @param string value     Value to test

        @return bool
        """

        if(type(date_value) == int):
            date_value = str(date_value)


        ## Order:
        # If * then true
        # If value is same as date_value then True
        # If contains '/' than test for increment
        # If contains '-' than test range 
        #
        # NOTE: increment before range because increment could contain dash
        if(value == '*'):
            return True
        elif(date_value == value):
            return True
        elif(self.isIncrementsOfRanges(value)): 
            return self.isInIncrementsOfRanges(date_value, value)
        elif(self.isRange(value)):
            return self.isInRange(date_value, value)
        else:
            return False

    
    def isRange(self, value):
        """
        Check if a value is a range (contains ONE 'dash')

        @param string value value to test

        @return bool
        """
        return (value.count('-') == 1)

    def isIncrementsOfRanges(self, value):
        """
        Check if a value is an increment of ranges (contains ONE 'slash')

        @param string value value to test

        @return bool
        """
        return (value.count('/') == 1)

    def isInRange(self, date_value, value):
        """
        Test if a value is within a range
        
        @param string date_value set date value
        @param string value      Value to test

        @return bool
        """

        try:
            (low, high) = value.split('-')
            low  = int(low)
            high = int(high)
            date_value = int(date_value)

            return (low <= date_value) and (high >= date_value)
        except ValueError, e: # Casting went wrong
            raise ValueError('value format is wrong', e)

    def isInIncrementsOfRanges(self, date_value, value):
        """
        Test if a value is within an increments of ranges (offset[-to]/step size)
       
        offset-to examples:
            */5 - Every 5
            0/5 - Every 5
            3/5 - 3 after 0 and every 5 after (3, 8, 13, 18, ....)
            3-33/5 - 3 after 0 and every 5 until 33 (3, 8, 13, 18, 23, 28, 33)

        @param string $dateValue Set date value
        @param string $value Value to test
        
        @return bool
        """
        try:
            (offset, step) = value.split('/')
            int_step = int(step)
            int_date_value = int(date_value)

            if (offset.strip() == '*') or (offset.strip() == '0'): # no offset
                return ( int_date_value % int_step == 0 )

            if(offset.count('-') == 0):
                int_offset = int(offset)
                int_offset_to = int_date_value
            elif(offset.count('-') == 1):
                (offset, offset_to) = offset.split('-')    
                int_offset = int(offset)
                int_offset_to = int(offset_to)
            else:
                raise ValueError('value format is wrong')

            # Out of range
            if (int_date_value < int_offset) or (int_date_value > int_offset_to):
                return False

            # Correct value to offset
            int_date_value = int_date_value-int_offset
            return ( int_date_value % int_step == 0)

        except Exception, e:
            raise ValueError('value format is wrong', e)

    def validate(self, value): 
        if (self._validate.match(str(value)) == None):
            raise InputValueError(error_message="Value validation failed", expected = self.__validate__, value = value, from_obj = self, result = self._validate.match(str(value)) )
        return True

    def isSatisfiedBy(self, date, value): 
        raise NotImplementedError('Method "isSatisfiedBy" is not implemented in %s' % self.__class__.__name__)
    def increment(self, date, invert = False): 
        raise NotImplementedError('Method "increment" is not implemented in %s' % self.__class__.__name__)

class InputValueError(Exception):
    def __init__(self, error_message, value = None, expected = None, from_obj = None, result = None):
        super(InputValueError, self).__init__(error_message)
        self.errmsg   = error_message
        self.value    = value
        self.expected = expected
        self.from_obj = from_obj
        self.result   = result
