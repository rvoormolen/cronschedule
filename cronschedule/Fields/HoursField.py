import AbstractField
import datetime

class HoursField(AbstractField.AbstractField):
    """
    Hours field. Allows: * / , - ? L #

    Based on https://github.com/mtdowling/cron-expression/blob/master/src/Cron/HoursField.php
    """
    __validate__ = "[-*,/0-9A-Z]+"

    def isSatisfiedBy(self, date, value):
        ## Find out if this is the last specific day of the month
        return self.isSatisfied( date.hour, value )

    def increment(self, date, invert = False):
        if(type(date) != datetime.datetime):
            raise ValueError()

        # TODO: Implement TZ information via : http://joelinoff.com/blog/?p=802
        if(invert == True):
            date = date-datetime.timedelta(hours = 1)
            date = date.replace(minute=59)
        else:
            date = date+datetime.timedelta(hours = 1)
            date = date.replace(minute=0)

        return date
    
__all__ = [ 'HoursField' ]
