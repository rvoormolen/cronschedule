import AbstractField
import datetime
from calendar import monthrange

class MonthField(AbstractField.AbstractField):
    """
    Month field. Allows: * / , - ? L #

    Based on https://github.com/mtdowling/cron-expression/blob/master/src/Cron/MonthField.php
    """
    __validate__ = "[-*,/0-9A-Z]+"

    _MONTHS = {
        'JAN': '1',  'FEB': '2',  'MAR': '3',
        'APR': '4',  'MAY': '5',  'JUN': '6',
        'JUL': '7',  'AUG': '8',  'SEP': '9',
        'OCT': '10', 'NOV': '11', 'DEC': '12'
    }

    def isSatisfiedBy(self, date, value):
        ## Convert text month to number month
        for m in self._MONTHS:
            value = value.upper().replace(m, self._MONTHS[m])

        return self.isSatisfied( date.month, value )

    def increment(self, date, invert = False):
        if(type(date) != datetime.datetime):
            raise ValueError()

        # Days in this month 
        days_this_month = monthrange(date.year, date.month)[1]

        if(invert == True):
            date = date-datetime.timedelta(days = days_this_month)
            days_this_month = monthrange(date.year, date.month)[1] # Reset days in month
            date = date.replace(day = days_this_month, hour = 23, minute = 59, second = 0)
        else:
            date = date+datetime.timedelta(days = days_this_month)
            date = date.replace(day = 1, hour = 0, minute = 0, second = 0)

        return date

    
__all__ = [ 'MonthField' ]
