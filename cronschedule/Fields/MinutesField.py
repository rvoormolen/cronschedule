import AbstractField
import datetime

class MinutesField(AbstractField.AbstractField):
    """
    Minutes field. Allows: * / , - ? L #

    Based on https://github.com/mtdowling/cron-expression/blob/master/src/Cron/MinutesField.php
    """

    __validate__ = "^[-*,/0-9A-Z]+$"

    def isSatisfiedBy(self, date, value):
        ## Find out if this is the last specific day of the month
        return self.isSatisfied( date.minute, value )

    def increment(self, date, invert = False):
        if(type(date) != datetime.datetime):
            raise ValueError()

        if(invert == True):
            return date-datetime.timedelta(0, 60, 0)
        else:
            return date+datetime.timedelta(0, 60, 0)

    
__all__ = [ 'MinutesField' ]
