import AbstractField
import datetime

class YearField(AbstractField.AbstractField):
    """
    Year field. Allows: * / , - ? L #

    Based on https://github.com/mtdowling/cron-expression/blob/master/src/Cron/YearField.php
    """
    __validate__ = "[*,/-0-9A-Z]+"

    def isSatisfiedBy(self, date, value):
        ## Find out if this is the last specific day of the month
        return self.isSatisfied( date.year, value )

    def increment(self, date, invert = False):
        if(type(date) != datetime.datetime):
            raise ValueError()

        if(invert == True):
            date = datetime.datetime(date.year-1, 12, 31, 23, 59, 0)
        else:
            date = datetime.datetime(date.year+1, 1, 1, 0, 0, 0)

        return date

__all__ = [ 'YearField' ]
