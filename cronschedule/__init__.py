from CronExpression import CronExpression
from FieldFactory   import FieldFactory
import Fields

# Based on https://github.com/mtdowling/cron-expression/blob/master/src/Cron

# @dict list of default mappings
_mappings = {
    '@yearly':   '0 0 1 1 *',
    '@annually': '0 0 1 1 *',
    '@monthly':  '0 0 1 * *',
    '@weekly':   '0 0 * * 0',
    '@daily':    '0 0 * * *',
    '@hourly':   '0 * * * *'
}

def factory(expression, fieldFactory = None):
    """
        Factory method to create a new CronExpression
        
        @param string expression The CRON expression to Create. There are
            several special predefined values which can be used to substitue the
            CRON expression:

            @yearly, @annually - Run once a year, midnight, Jan. 1 - 0 0 1 1 *
            @monthly - Run once a month, midnight, first of month - 0 0 1 * *
            @weekly - Run once a week, midnight on Sun - 0 0 * * 0
            @daily - Run once a day, midnight - 0 0 * * *
            @hourly - Run once an hour, first minute - 0 * * * *
        @param fieldFactory, fieldFactory to use

        @return CronExpression

        Cron format : minute hour day_of_month month day_of_week

        NOTE: This is originally a static function in the class CronExpression
    """
    if (fieldFactory != None):
        if not isinstance(fieldFactory, FieldFactory):
            raise ValueError("FieldFactory not of type FieldFactory")

    if (fieldFactory == None):
        fieldFactory = FieldFactory()

    # Check if it is a defined mapping
    if expression in _mappings.keys():
        expression = _mappings[expression]

    return CronExpression(expression, fieldFactory)

# CronSchedule version
__version__ = "0.1"
