import Fields

class FieldFactory():
    _fields = [ None, None, None, None, None, None ]

    def get_field(self, position):
        try:
            field = self._fields[position]
        except IndexError:
            raise IndexError('{position} is not a valid position'.format(position = position))
        else:
            if(field == None):
                if(position == 0):   
                    self._fields[position] = Fields.MinutesField() 
                elif(position == 1):
                    self._fields[position] = Fields.HoursField() 
                elif(position == 2):
                    self._fields[position] = Fields.DayOfMonthField() 
                elif(position == 3):
                    self._fields[position] = Fields.MonthField() 
                elif(position == 4):
                    self._fields[position] = Fields.DayOfWeekField() 
                elif(position == 5):
                    self._fields[position] = Fields.YearField() 
       
            return self._fields[position]

    def getField(self, position):
        return self.get_field(position)
