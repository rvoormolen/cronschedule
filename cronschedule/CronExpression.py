from FieldFactory import FieldFactory
from Fields.AbstractField import InputValueError
import datetime
## Python rewrite of : https://github.com/mtdowling/cron-expression/tree/master/src/Cron

class CronExpression( ):
    _MINUTE  = 0
    _HOUR    = 1
    _DAY     = 2
    _MONTH   = 3
    _WEEKDAY = 4
    _YEAR    = 5

    # @list CRON expression parts
    cron_parts = [ '*', '*', '*', '*', '*' ]

    # @list CRON field factory
    field_factory = None

    # @list Order in which to test cron parts
    _order = [ _YEAR, _MONTH, _DAY, _WEEKDAY, _HOUR, _MINUTE ]

    def __init__(self, expression, fieldfactory):
        if not isinstance(fieldfactory, FieldFactory):
            raise ValueError("FieldFactory not of type FieldFactory")

        self.field_factory = fieldfactory
        self.set_expression(expression)

    def set_expression(self, value):
        """
            Set CRON Expression

            @param value string CRON expression
            @return CronExpression self (return self)
        """
        parts = value.split(" ")
        if( len(parts) != 5 ):
            raise ValueError("{} is not a valid CRON expression".format(value))

        for position, part in enumerate(parts):
            self.set_part(position, part)

        return self

    def set_part(self, position, value):
        """
            Set part of argument

            @param int    position Position to store
            @param string value    Value to store
            
            @return CronExpression self (return self)
        """
        try:
            self.field_factory.get_field(position).validate(value)
        except InputValueError:
            raise ValueError("Invalid CRON field value {value} at position {position}".format(value=value, position=position))
        else:
            self.cron_parts[position] = str(value)

            return self

    def get_next_run_date(self, current_time = 'now', nth = 0, allow_current_date = False):
        """
            Get next run date

            @param  string|datetime.datetime    current_time        (Optional) Start moment to search from
            @param  int                         nth                 (Optional) Return the nth occurance
            @param  bool                        allow_current_date  (Optional) Allow to match on current date

            @return datetime.datetime next run date
        """
        return self.get_run_date(current_time, nth, False, allow_current_date)

    def get_previous_run_date(self, current_time = 'now', nth = 0, allow_current_date = False):
        """
            Get previous run date

            @param  string|datetime.datetime    current_time        (Optional) Start moment to search from
            @param  int                         nth                 (Optional) Return the nth occurance
            @param  bool                        allow_current_date  (Optional) Allow to match on current date

            @return datetime.datetime previous run date
        """
        return self.get_run_date(current_time, nth, True, allow_current_date)

    def get_multiple_run_dates(self, total, current_time = 'now', invert = False, allow_current_date = False):
        """
            Return list of run dates

            @param  int                         total               Total number of run dates to return
            @param  string|datetime.datetime    current_time        (Optional) Start moment to search from
            @param  bool                        invert              (Optional) Search backwards
            @param  bool                        allow_current_date  (Optional) Allow to match on current date

            @return list List of run dates
        """
        matches = [ ]
        while len(matches) < total:
            matches.append( self.get_run_date( current_time, 1, invert, allow_current_date ) )
            current_time = matches[-1]  # Reset search start point
            allow_current_date = False
        return matches

    def get_expression(self, part = None):
        """
            Return (part of) CRON expression

            @param int  part    (Optional) part to return

            @return string (part of) expression
            @return None when part is unknown
        """
        if(part == None):
            return " ".join(self.cron_parts)
        elif(part < 5):
            return str(self.cron_parts[part])
        else:
            return None

    def __str__(self):
        return self.get_expression()

    
    def get_run_date(self, current_time = None, nth = 0, invert = False, allow_current_date = False):
        """
        Get the next or previous run date of the expression relative to a date

        For sake of DST errors al local times are recalculated to UTC

        @param datetime|string current_time       (optional) Relative calculation date
        @param int             nth                (optional) Number of matches to skip before returning 
        @param bool            invert             (optional) Set to True to search back in time
        @param bool            allow_current_date (optional) Set to TRUE to return the current
                                                    date if it matches the cron expression

        @return datetime.datetime
        @raises Exception on too many iterations
        @raises ValueError if input is not valid
        """

        current_date = self._parse_current_time(current_time)

        # Reset to datetime without seconds and other non-relevant data
        current_date = datetime.datetime(current_date.year, current_date.month, current_date.day, 
                                         current_date.hour, current_date.minute)
        next_run     = current_date

        # Hard limit to prevent searching for an impossible date
        runs = 0
        while runs < 1000:
            for position in self._order:
                part = self.get_expression(position)
                if( part == None ):
                    continue

                # Get the Field object to validate this part
                field = self.field_factory.get_field(position)

                for listPart in part.split(","):
                    if(field.isSatisfiedBy(next_run, listPart.strip())):
                        break
                else: # Got no break in for loop
                    # No satisfaction, raise the next_run with what the field supplies
                    next_run = field.increment(next_run, invert)
                    break # for position in self._order:
       
            else: # Got no break on any position
                if ( (allow_current_date == True) and (current_date == next_run) ) or nth == 0:
                    return next_run
                else:
                    nth -= 1
                    next_run = self.field_factory.get_field(0).increment(next_run, invert)

            runs += 1

        raise Exception("Could not find a date with supplied expression in %d runs" % runs)

    def _parse_current_time(self, current_time):
        if type(current_time) == datetime.datetime:
            current_date = current_time
        elif type(current_time) == str and current_time.lower() == 'now':
            current_date = datetime.datetime.now()
        elif current_time == None:
            current_date = datetime.datetime.now()
        else:
            raise ValueError("current_time should contain a valid string (now) or should provide an datetime.datetime object")

        return current_date
